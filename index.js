//1. Solution
	function cube(){
		const num = 2**3;
		console.log(`The cube of 2 is : ${num}`);
	} cube();


//2. Solution
// Declare 1st the value of array in a traditional way
	const address = ["Saint Mary St.", "Cubao", "Quezon City"];
// then destructuring the array
	const [street, place, city] = address;
	console.log(`I live at ${street} ${place} ${city}.`);


//3. Declare 1st the object in traditional Way
	const animal = {
		name: "lolong",
		kind: "saltwater Crocodile",
		weight: "1075 kgs.",
		measurement: "20 ft 3 inches"
	}

// Object Destructuring
	const { name, kind, weight, measurement } = animal;
// then log print it in console
	console.log(`${name} was a ${kind}. He weighed at ${weight} with a measurement of ${measurement}`);

// 4. declare the array 1st in traditional way
	const numbers = [1,2,3,4,5];
// then print in the console using forEach() & arrow Function
	numbers.forEach((number) => {
	console.log(`${number}`);
});

// 5.
// Solution
// Declare first a Object Constructor
	class Dog {
		constructor (name, age, breed) {
			this.name = name;
			this.age = age;
			this.breed = breed;
		}
	}
// then instantiate the value property of an object
	const myDog = new Dog ("Coffee", 5, "Belgian Mallinois");
	console.log(myDog);
